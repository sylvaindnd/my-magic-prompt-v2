#!/bin/bash

logged=0
my_password="password"
my_login="login"

verify_login () {
    echo -n "Login : "
    read login
    echo -n "Password : "
    read -s password
    if [ $login == $my_login ] && [ $password == $my_password ]; then
        logged=1
        echo ""
        prompt
    else
        echo ""
        echo "Invalid login or password"
        exit
    fi
}

about (){
    echo "Simplify command for noobs"
}

quit (){
    exit
}

cd_dir() {
    echo -n "Select directory : "
    read directory
    if [ -z $directory ]; then
        echo "Enter a directory please"
        cd_dir
    fi   
    mkdir -p $directory
    cd $directory    
}

ls_dir (){
    echo -n "Select directory (leave blank for this directory) : "
    read directory
    if [ -d $directory ]; then
        ls -a $directory
    else
        echo "This directory doesn't exist"
    fi
}

open (){
    echo -n "Enter file name : "
    read directory

    if [ ! -z $directory ]; then
        vim $directory
        echo "The file has been created"
    else
        echo "Enter valid file name"
    fi
}

rm_file (){
    echo -n "Select path : "
    read directory
    if [ -a $directory ] && [ ! -z $directory ]; then
        rm $directory
        echo "The file has been removed"
    else
        echo "The path/file doesn't exist"
    fi
}

rmd_dir (){
    echo -n "Select directory to remove : "
    read directory
    if [ -d $directory ] && [ ! -z $directory ]; then
        rm -r $directory
        echo "The folder has been removed"
    else
        echo "This folder doesn't exist"
    fi
}

version (){
    echo "main version 1.0.0"
}

profile() {
    echo "Hello, my name is Sylvain Dendele, my age is 20y, my email is d@d.fr"
}

age (){
    echo -n "Enter your age : "
    read my_age
    if [[ $my_age == ?(-)+([0-9]) ]] && (( $my_age > 0 )); then 
        if (( $my_age >= 18)); then
            echo "Your are an adulte now !"
        else 
            echo "You are too small for this kind of thing"
        fi
    else
        echo "Enter your real age please"
        age
    fi
}

passw() {
    echo -n "Enter your new password : "
    read -s pass_1
    echo ""
    echo -n "Enter again your password : "
    echo ""
    read -s pass_2
    if [ $pass_1 == $pass_2 ]; then
        my_password=$pass_1
        echo "Your password has been changed !"
    else
        echo "The passwords don't match"
    fi
}

pw_dir (){
    pw_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
    echo $pw_dir
}

hour (){
    date +"%H:%M:%S"
}

smtp() {
    echo -n "To : "
    read emailTo

    echo -n "Subject : "
    read subjectBody

    echo -n "Message : "
    read messageBody

    mail -s "$subjectBody" "$emailTo" <<< "$messagebody"
    echo "Mail send !"
}

rps (){
    round=1
    max_round=3
    echo "Play Rock Paper Scissors with your friend !"
    sleep 1

    p1_points=0
    p2_points=0

    psw="Equality !"
    p1w="Player 1 WIN !"
    p2w="Player 2 WIN !"

    while (( $round <= $max_round ))
    do
        echo ""
        echo "======="
        echo "ROUND $round"
        echo "======="
        echo ""

        echo -n "-Player 1 : ['r':rock],['p':paper],['s':scissors] ? "
        read p1
        while [ $p1 != "r" ] && [ $p1 != "p" ] && [ $p1 != "s" ] && [ $p1 != "mega" ]
        do
            echo -n "-Player 1 : ['r':rock],['p':paper],['s':scissors] ? "
            read p1
        done

        echo ""

        echo -n "-Player 2 : ['r':rock],['p':paper],['s':scissors] ? "
        read p2
        while [ $p2 != "r" ] && [ $p2 != "p" ] && [ $p2 != "s" ] && [ $p1 != "mega" ]
        do
            echo -n "-Player 1 : ['r':rock],['p':paper],['s':scissors] ? "
            read p2
        done

        echo ""

        if [ $p1 == "r" ] && [ $p2 == "r" ]; then
            p1_points=$(($p1_points+0))
            p2_points=$(($p2_points+0))
            echo $psw
        fi
        if [ $p1 == "r" ] && [ $p2 == "p" ]; then
            p1_points=$(($p1_points+0))
            p2_points=$(($p2_points+1))
            echo $p2w
        fi
        if [ $p1 == "r" ] && [ $p2 == "s" ]; then
            p1_points=$(($p1_points+1))
            p2_points=$(($p2_points+0))
            echo $p1w
        fi

        if [ $p1 == "p" ] && [ $p2 == "r" ]; then
            p1_points=$(($p1_points+1))
            p2_points=$(($p2_points+0))
            echo $p1w
        fi
        if [ $p1 == "p" ] && [ $p2 == "p" ]; then
            p1_points=$(($p1_points+0))
            p2_points=$(($p2_points+0))
            echo $psw
        fi
        if [ $p1 == "p" ] && [ $p2 == "s" ]; then
            p1_points=$(($p1_points+0))
            p2_points=$(($p2_points+1))
            echo $p2w
        fi    

        if [ $p1 == "s" ] && [ $p2 == "r" ]; then
            p1_points=$(($p1_points+0))
            p2_points=$(($p2_points+1))
            echo $p2w
        fi
        if [ $p1 == "s" ] && [ $p2 == "p" ]; then
            p1_points=$(($p1_points+1))
            p2_points=$(($p2_points+0))
            echo $p1w
        fi
        if [ $p1 == "s" ] && [ $p2 == "s" ]; then
            p1_points=$(($p1_points+0))
            p2_points=$(($p2_points+0))
            echo $psw
        fi

        if [ $p1 == "mega" ]; then
            p1_points=10
            echo "Thunder attack from Player 1"
        fi
        if [ $p2 == "mega" ]; then
            p2_points=10
            echo "Thunder attack from Player 2"
        fi

        round=$(($round+1))
    done

    echo ""
    echo "----------------"
    echo ""

    if [ $p1_points == $p2_points ]; then        
        echo "EQUALITY !"        
    elif [ $p1_points -gt $p2_points ]; then
        echo "Player 1 WIN THE ROCK PAPER SCISSORS !"  
    else
        echo "Player 2 WIN THE ROCK PAPER SCISSORS !"  
    fi

    echo ""
    echo "Player 1 : $p1_points pts"
    echo "Player 2 : $p2_points pts"
    echo ""

}

help (){
    echo "[ help ] find help"
    echo "[ ls ] list files hidden and visible"
    echo "[ rm ] remove file"
    echo "[ Rmd ] delete folder"
    echo "[ about ] describe this script"
    echo "[ version ] get current version"
    echo "[ age ] enter your age"
    echo "[ quit ] quit the program"
    echo "[ profile ] display personal informations"
    echo "[ passw ] gives you the possibility to change your password "
    echo "[ cd ] navigate in folders"
    echo "[ pwd ] display current directory"
    echo "[ hour ] give you current hour"
    echo "[ httpget ] get source code of web page"
    echo "[ smtp ] send mail"
    echo "[ open ] open file in editor"
}

prompt () {
    echo -n "command : "
    read command
    case $command in 
        about ) about;;
        quit ) quit;;
        ls ) ls_dir;;
        cd ) cd_dir;;
        open ) open;;
        rm ) rm_file;;
        rmd | rmdir ) rmd_dir;;
        version | --v | vers ) version;;
        profile ) profile;;
        age ) age;;
        passw ) passw;;
        pwd ) pw_dir;;
        hour ) hour;;
        httpget ) httpget;;
        smtp ) smtp;;
        rps ) rps;;
        help ) help;;
        * ) echo "invalid command";;
    esac

    prompt
}

httpget () {
    echo -n "Site URL : "
    read url
    echo -n "File name : "
    read filename

    wget -nv -O $filename $url
    echo "$url was save as $filename"
}

main () {
    if [ $logged == 0 ]; then 
        verify_login
    else
        prompt
    fi
}

main